$(function(){
    // #で始まるアンカーをクリックした場合に処理
    $('a[href^="#"]#toTop').on('click', function() {
        // スムーススクロール
        var speed = 400;// ミリ秒
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
return false;
    });
});

$(function(){
    // #で始まるアンカーをクリックした場合に処理
    $('.info a[href^="#"]').on('click', function() {
        // スムーススクロール
        var speed = 400;// ミリ秒
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');

        // 開閉パネルが閉じていたら
        if($(href).children('.hidden').css('display') == 'none'){
          // 同時に開閉イベントを実行
          $(href).children('#sectionAbout h2').trigger('click');
        }
        return false;
    });

});

$(function(){
// 見出しをクリックするとコンテンツを開閉する
    $('#sectionAbout h2').on('click', function() {
//$(this).next('div:not(:animated)').fadeToggle();
        $(this).next().toggle(100);
    });
});


$(function(){
    // #で始まるアンカーをクリックした場合に処理
    $('.info a[href^="#"]').on('click', function() {
        // スムーススクロール
        var speed = 400;// ミリ秒
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');

        // 開閉パネルが閉じていたら
        if($(href).children('.hidden').css('display') == 'none'){
          // 同時に開閉イベントを実行
          $(href).children('#sectionPortfolio h2').trigger('click');
        }
        return false;
    });
});


$(function(){
    // 見出しをクリックするとコンテンツを開閉する
    $('#sectionPortfolio h2').on('click', function() {
    $(this).next('div:not(:animated)').fadeToggle();
        $(this).children('.hidden').toggle(100);
    });
});


$(function(){
  $('#sectionAbout .close').click(function(){
    // 引数には開閉する速度を指定します
    $('#sectionAbout .hidden').slideToggle('normal');
  });
});

$(function(){
    $('#sectionPortfolio .close').click(function(){
        // 引数には開閉する速度を指定します
        $('#sectionPortfolio .hidden').fadeToggle('normal');
    });
});

$(function(){
  $('footer h2').on('click' , function(){
    // 引数には開閉する速度を指定します
    $(this).next().slideToggle('normal');
  });
});
// footerMenu

$(function(){
  $('#menuBottom span').on('click',function(){
    // 引数には開閉する速度を指定します
    $(this).next().fadeToggle('normal');
  });
});


$(function(){
    // #で始まるアンカーをクリックした場合に処理
    $('#bottomAbout').on('click', function() {
        // スムーススクロール
        var speed = 400;// ミリ秒
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        $(this).parent().fadeToggle('normal');
        // 開閉パネルが閉じていたら
        if($(href).children('.hidden').css('display') == 'none'){
          // 同時に開閉イベントを実行
          $(href).children('#sectionAbout h2').trigger('click');
        }
        return false;
    });
});


$(function(){
    // #で始まるアンカーをクリックした場合に処理
    $('#bottomPortfolio').on('click', function() {
        // スムーススクロール
        var speed = 400;// ミリ秒
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        $(this).parent().fadeToggle('normal');
        // 開閉パネルが閉じていたら
        if($(href).children('.hidden').css('display') == 'none'){
          // 同時に開閉イベントを実行
          $(href).children('#sectionPortfolio h2').trigger('click');
        }
        return false;
    });

});

$(function(){
    // #で始まるアンカーをクリックした場合に処理
    $('#menuBottom #bottomMain').on('click', function() {
        // スムーススクロール
        var speed = 400;// ミリ秒
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        $(this).parent().fadeToggle('normal');
        return false;
    });
});


$(function(){
    //$('#sectionPortfolio .box').css('display','none');
    $('#sectionPortfolio h3').click(function(){
        // 引数には開閉する速度を指定します
          $(this).next().next('.box').slideToggle(250).toggleClass('flex');

    });
});

$(function(){
    $('#sectionPortfolio h4').click(function(){
        // 引数には開閉する速度を指定します
        $(this).next('.box').slideToggle(250).toggleClass('flex');
    });
});
